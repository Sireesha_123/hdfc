package com.bank.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.dto.LoginDto;
import com.bank.dto.ResponseDto;
import com.bank.service.Loginservice;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("/account")
@Validated
public class Logincontroller {

	private Loginservice loginservice;

	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@Valid @RequestBody LoginDto loginDto) {
		return new ResponseEntity<>(loginservice.login(loginDto), HttpStatus.OK);
}
}