package com.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.entity.CustomerRegistration;

public interface LoginRepository extends JpaRepository<CustomerRegistration, String> {

	CustomerRegistration findByCustomerId(String customerId);

}
