package com.bank.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bank.controller.Logincontroller;
import com.bank.dto.LoginDto;
import com.bank.dto.ResponseDto;
import com.bank.service.Loginservice;


@ExtendWith(SpringExtension.class)
class LoginControllerTest {
   @Test
   void testUserLogin() {
       // Mock dependencies
       Loginservice loginService = mock(Loginservice.class);
       when(loginService.login(any())).thenReturn(new ResponseDto("Logged In successfully", 201));
       Logincontroller loginController = new Logincontroller(loginService);
       // Create a loginDto
       LoginDto loginDto = new LoginDto("testCustomerId", "testPassword");
       // Call the userLogin method
       ResponseEntity<ResponseDto> responseEntity = loginController.userLogin(loginDto);
       // Assert the response
       assertEquals("Logged In successfully", responseEntity.getBody().getMessage());
       assertEquals(201, responseEntity.getBody().getHttpStatus());
   }
}