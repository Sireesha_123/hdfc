package com.bank.demo.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bank.controller.TransactionController;
import com.bank.dto.loginresponse;
import com.bank.entity.Beneficiary;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.InsufficientBalanceException;
import com.bank.repository.BeneficiaryRepository;
import com.bank.repository.CustomerRegistrationRepository;

class TransactionTest {
	@Mock
	private CustomerRegistrationRepository customerRepository;
	@Mock
	private BeneficiaryRepository beneficiaryRepository;
	@InjectMocks
	private TransactionController transaction;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void transferAmount_Success() {
		String customerId = "123";
		Long beneficiaryId = 456L;
		double amount = 100.0;
		CustomerRegistration customer = new CustomerRegistration();
		customer.setBalance(200.0);
		Beneficiary beneficiary = new Beneficiary();
		when(customerRepository.findByCustomerId(customerId)).thenReturn(Optional.of(customer));
		when(beneficiaryRepository.findByBeneficiaryId(beneficiaryId)).thenReturn(beneficiary);
		ResponseEntity<loginresponse> responseEntity = transaction.transferAmount(customerId, beneficiaryId, amount);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("Amount transferred successfully.", responseEntity.getBody().getMessage());
		assertEquals(100.0, customer.getBalance());
		verify(customerRepository, times(1)).save(customer);
	}

	@Test
	void transferAmount_CustomerNotFound() {
		String customerId = "123";
		Long beneficiaryId = 456L;
		double amount = 100.0;
		when(customerRepository.findByCustomerId(customerId)).thenReturn(Optional.empty());
		ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
				() -> transaction.transferAmount(customerId, beneficiaryId, amount));
		assertEquals("Customer not found with ID: " + customerId, exception.getMessage());
		verify(customerRepository, never()).save(any());
	}

	@Test
	void transferAmount_BeneficiaryNotFound() {
		String customerId = "123";
		Long beneficiaryId = 456L;
		double amount = 100.0;
		CustomerRegistration customer = new CustomerRegistration();
		when(customerRepository.findByCustomerId(customerId)).thenReturn(Optional.of(customer));
		when(beneficiaryRepository.findByBeneficiaryId(beneficiaryId)).thenReturn(null);
		ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class,
				() -> transaction.transferAmount(customerId, beneficiaryId, amount));
		assertEquals("Beneficiary not found with ID: " + beneficiaryId, exception.getMessage());
		verify(customerRepository, never()).save(any());
	}

	@Test
	void transferAmount_InsufficientBalance() {
		String customerId = "123";
		Long beneficiaryId = 456L;
		double amount = 300.0;
		CustomerRegistration customer = new CustomerRegistration();
		customer.setBalance(200.0);
		Beneficiary beneficiary = new Beneficiary();
		when(customerRepository.findByCustomerId(customerId)).thenReturn(Optional.of(customer));
		when(beneficiaryRepository.findByBeneficiaryId(beneficiaryId)).thenReturn(beneficiary);
		InsufficientBalanceException exception = assertThrows(InsufficientBalanceException.class,
				() -> transaction.transferAmount(customerId, beneficiaryId, amount));
		assertEquals("Insufficient balance for customer: " + customerId, exception.getMessage());
		verify(customerRepository, never()).save(any());
	}
}