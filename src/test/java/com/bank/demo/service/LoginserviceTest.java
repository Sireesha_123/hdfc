package com.bank.demo.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bank.dto.LogInStatus;
import com.bank.dto.LoginDto;
import com.bank.dto.ResponseDto;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ProfileNotFoundException;
import com.bank.repository.CustomerRegistrationRepository;
import com.bank.repository.LoginRepository;
import com.bank.service.Loginservice;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class LoginserviceTest {
	@Mock
	private LoginRepository loginRepository;
	@Mock
	private CustomerRegistrationRepository customerRegistrationRepository;
	@InjectMocks
	private Loginservice loginservice;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testLogin_Successful() {

		LoginDto loginDto = new LoginDto("customerId", "password");
		CustomerRegistration customerRegistration = new CustomerRegistration();
		customerRegistration.setPassword("password");
		when(customerRegistrationRepository.findByCustomerIdAndPassword("customerId", "password"))
				.thenReturn(customerRegistration);

		ResponseDto response = loginservice.login(loginDto);

		assertNotNull(response);
		assertEquals("Logged In successfully", response.getMessage());
		assertEquals(201, response.getHttpStatus());
		assertEquals(LogInStatus.LOGIN, customerRegistration.getLoggedIn());
		verify(customerRegistrationRepository, times(1)).save(customerRegistration);
	}

	@Test
	void testLogin_InvalidCredentials() {

		LoginDto loginDto = new LoginDto("customerId", "password");
		when(customerRegistrationRepository.findByCustomerIdAndPassword("customerId", "password")).thenReturn(null);

		assertThrows(ProfileNotFoundException.class, () -> loginservice.login(loginDto));
	}
}