package com.bank.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bank.dto.BeneficiaryDTO;
import com.bank.dto.BeneficiaryResponseDTO;
import com.bank.entity.CustomerRegistration;
import com.bank.exception.ResourceNotFound;
import com.bank.repository.BeneficiaryRepository;
import com.bank.repository.CustomerRegistrationRepository;
import com.bank.service.BeneficiaryService;

@ExtendWith(MockitoExtension.class)
class BeneficiaryServiceTest {

	@Mock
	private BeneficiaryRepository beneficiaryRepository;

	@Mock
	private CustomerRegistrationRepository registrationRepository;

	@InjectMocks
	private BeneficiaryService beneficiaryService;

	private BeneficiaryDTO beneficiaryDTO;
	private CustomerRegistration registration;

	@BeforeEach
	void setUp() {

		beneficiaryDTO = new BeneficiaryDTO();
		beneficiaryDTO.setBeneficiaryName("John Doe");
		beneficiaryDTO.setBeneficiaryAccountNo("1234567890");
		beneficiaryDTO.setBeneficiaryBankName("Bank of XYZ");
		beneficiaryDTO.setBeneficiaryBranch("Branch ABC");
		beneficiaryDTO.setBeneficiaryIFSCCode("XYZ1234567");

		registration = new CustomerRegistration();
		registration.setCustomerId("123");
	}

	@Test
	void testAddBeneficiary_Success() {

		when(registrationRepository.findByCustomerId("123")).thenReturn(Optional.of(registration));
		when(beneficiaryRepository.existsByBeneficiaryAccountNo("1234567890")).thenReturn(false);

		BeneficiaryResponseDTO responseDTO = beneficiaryService.addBeneficiary("123", beneficiaryDTO);

		assertNotNull(responseDTO);
		assertEquals("Beneficiary Added Successfully...", responseDTO.getMessage());
		assertEquals("123", responseDTO.getCustomerId());

		verify(beneficiaryRepository, times(1)).save(any());
	}

	@Test
	void testAddBeneficiary_CustomerNotFound() {

		when(registrationRepository.findByCustomerId("123")).thenReturn(Optional.empty());

		assertThrows(ResourceNotFound.class, () -> {
			beneficiaryService.addBeneficiary("123", beneficiaryDTO);
		});

		verify(beneficiaryRepository, never()).save(any());
	}

	@Test
	void testAddBeneficiary_BeneficiaryAlreadyExists() {

		when(registrationRepository.findByCustomerId("123")).thenReturn(Optional.of(registration));
		when(beneficiaryRepository.existsByBeneficiaryAccountNo("1234567890")).thenReturn(true);

		assertThrows(ResourceNotFound.class, () -> {
			beneficiaryService.addBeneficiary("123", beneficiaryDTO);
		});

		verify(beneficiaryRepository, never()).save(any());
	}
}